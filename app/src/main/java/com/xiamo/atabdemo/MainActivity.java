package com.xiamo.atabdemo;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.xiamo.atab.ATab;
import com.xiamo.atab.ATabItem;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private ATab aTab;
    private TextView statusTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        aTab = (ATab) findViewById(R.id.atab);
        statusTv = (TextView) findViewById(R.id.status_tv);
        findViewById(R.id.msg_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aTab.setMsg(3, null);
            }
        });
        findViewById(R.id.select_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aTab.tabClick(2);
            }
        });
        aTab.setSelect(0);


        aTab.addItem(new ATabItem.Builder(this).title("首页")
                .uncheckIcon(getDrawable(R.mipmap.main_tab_home_no_select))
                .checkIcon(getDrawable(R.mipmap.main_tab_home_select))
                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
                .checkRadius(0)
                .upHeight(12)
                .create());
        aTab.addItem(new ATabItem.Builder(this).title("淘花榜")
                .uncheckIcon(getDrawable(R.mipmap.main_tab_taohua_noselect))
                .checkIcon(getDrawable(R.mipmap.main_tab_taohua_select))
                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
                .checkRadius(0)
                .upHeight(12)
                .create());
        aTab.addItem(new ATabItem.Builder(this).title("柚子广场")
                .uncheckIcon(getDrawable(R.mipmap.main_tab_shaddock_no_select))
                .checkIcon(getDrawable(R.mipmap.main_tab_shaddock_select))
                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
                .checkRadius(0)
                .upHeight(12)
                .create());
        aTab.addItem(new ATabItem.Builder(this).title("社区")
                .uncheckIcon(getDrawable(R.mipmap.main_tab_community_no_select))
                .checkIcon(getDrawable(R.mipmap.main_tab_community_select))
                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
                .checkRadius(0)
                .upHeight(12)
                .create());
        aTab.addItem(new ATabItem.Builder(this).title("我的")
                .uncheckIcon(getDrawable(R.mipmap.main_tab_mine_no_select))
                .checkIcon(getDrawable(R.mipmap.main_tab_mine_select))
                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
                .checkRadius(0)
                .upHeight(12)
                .create());

        aTab.setOnItemSelectListener(new ATab.OnItemSelectListener() {
            @Override
            public void onItemSelect(int nowPos, int prePos) {
                statusTv.setText("当前选中：" + nowPos);
                Log.e("-----", nowPos + "---" + prePos);

            }
        });
    }

    private static final int MIN_CLICK_DELAY_TIME = 500;
    private static long lastClickTime;
    public boolean isInvalidClick() {
//        long time = System.currentTimeMillis();
//        if ((System.currentTimeMillis() - lastClickTime) < MIN_CLICK_DELAY_TIME) {
//            return true;
//        }else {
//            lastClickTime = time;
//        }
        return true;
    }
}

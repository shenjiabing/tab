package com.xiamo.atabdemo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.xiamo.atab.ATab;

import androidx.appcompat.app.AppCompatActivity;
import me.jessyan.autosize.internal.CancelAdapt;

public class MainNewActivity extends AppCompatActivity implements CancelAdapt {

    private ATab aTab;
    private TextView statusTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_main);
        aTab = (ATab) findViewById(R.id.atab);
        statusTv = (TextView) findViewById(R.id.status_tv);
        findViewById(R.id.msg_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aTab.setMsg(3, null);
            }
        });
        findViewById(R.id.select_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aTab.tabClick(2);
            }
        });
        aTab.setSelect(0);
        findViewById(R.id.tab1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInvalidClick()){
                    statusTv.setText("当前选中：" + "首页");
                }
                aTab.tabClick(0);
            }
        });
        findViewById(R.id.tab2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInvalidClick()){
                    statusTv.setText("当前选中：" + "淘花榜");
                    aTab.tabClick(1);
                }

            }
        });
        findViewById(R.id.tab3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInvalidClick()){
                    statusTv.setText("当前选中：" + "柚子广场");
                    aTab.tabClick(2);
                }

            }
        });
        findViewById(R.id.tab4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusTv.setText("当前选中：" + "社区");
                aTab.tabClick(3);
                showV7Dialog();

            }
        });
        findViewById(R.id.tab5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusTv.setText("当前选中：" + "我的");
                aTab.tabClick(4);
                showDialog();
            }
        });
        //        aTab.addItem(new ATabItem.Builder(this).title("首页")
        //                .uncheckIcon(getDrawable(R.mipmap.main_tab_home_no_select))
        //                .checkIcon(getDrawable(R.mipmap.main_tab_home_select))
        //                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
        //                .checkRadius(0)
        //                .upHeight(12)
        //                .create());
        //        aTab.addItem(new ATabItem.Builder(this).title("淘花榜")
        //                .uncheckIcon(getDrawable(R.mipmap.main_tab_taohua_noselect))
        //                .checkIcon(getDrawable(R.mipmap.main_tab_taohua_select))
        //                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
        //                .checkRadius(0)
        //                .upHeight(12)
        //                .create());
        //        aTab.addItem(new ATabItem.Builder(this).title("柚子广场")
        //                .uncheckIcon(getDrawable(R.mipmap.main_tab_shaddock_no_select))
        //                .checkIcon(getDrawable(R.mipmap.main_tab_shaddock_select))
        //                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
        //                .checkRadius(0)
        //                .upHeight(12)
        //                .create());
        //        aTab.addItem(new ATabItem.Builder(this).title("社区")
        //                .uncheckIcon(getDrawable(R.mipmap.main_tab_community_no_select))
        //                .checkIcon(getDrawable(R.mipmap.main_tab_community_select))
        //                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
        //                .checkRadius(0)
        //                .upHeight(12)
        //                .create());
        //        aTab.addItem(new ATabItem.Builder(this).title("我的")
        //                .uncheckIcon(getDrawable(R.mipmap.main_tab_mine_no_select))
        //                .checkIcon(getDrawable(R.mipmap.main_tab_mine_select))
        //                .uncheckColor(R.color.color_main_tab_no_select).checkColor(R.color.color_main_tab_select)
        //                .checkRadius(0)
        //                .upHeight(12)
        //                .create());

        //        aTab.setOnItemSelectListener(new ATab.OnItemSelectListener() {
        //            @Override
        //            public void onItemSelect(int nowPos, int prePos) {
        //                statusTv.setText("当前选中：" + nowPos);
        //                Log.e("-----", nowPos + "---" + prePos);
        //
        //            }
        //        });
    }

    private static final int MIN_CLICK_DELAY_TIME = 500;
    private static long lastClickTime;
    public boolean isInvalidClick() {
//        long time = System.currentTimeMillis();
//        if ((System.currentTimeMillis() - lastClickTime) < MIN_CLICK_DELAY_TIME) {
//            return true;
//        }else {
//            lastClickTime = time;
//        }
        return true;
    }


    private void showV7Dialog(){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this, R.style.BDAlertDialog);
        dialog.setTitle("提示").setMessage("确定要注销登录吗？");
        dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                toast("确定");
            }
        });
        dialog.setNegativeButton("取消", null);
        dialog.show();
    }


    private void showDialog(){
        final Dialog dialog=new Dialog(this,R.style.BDAlertDialog);
//        View view= LayoutInflater.from(this).inflate(R.layout.dialog_confirm_hint,null,false);
        View view= LayoutInflater.from(this).inflate(R.layout.dialog_confirm_hint,null);
        view.findViewById(R.id.tvLeft).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toast("取消");
                dialog.dismiss();
            }
        });
        view.findViewById(R.id.tvRight).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toast("确定");
                dialog.dismiss();
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    private void toast(String str){
        Toast.makeText(this,str,Toast.LENGTH_SHORT).show();
    }
}
